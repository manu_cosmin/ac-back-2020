const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10),cnp VARCHAR(13), varsta VARCHAR(3))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    gen: req.body.gen,
  };
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ
  var today = new Date();
  if (!bilet.nume || !bilet.prenume || !bilet.telefon || !bilet.email || !bilet.data_inceput || !bilet.data_sfarsit || !bilet.cnp || !bilet.varsta) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  }else{
    if(bilet.nume.length < 3 || bilet.nume.length > 30){
      console.log("Nume invalid!");
      error.push("Nume invalid!");
    }else if(!bilet.nume.match("^[A-Za-z]+$")){
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if(bilet.prenume.length <3 || bilet.prenume.length >30){
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    }else if(!bilet.prenume.match("^[A-Za-z]+$")){
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if(bilet.telefon.length != 10){
      console.log("NUmarul de telefon trebuie sa aiba 10 cifre");
      error.push("NUmarul de telefon trebuie sa aiba 10 cifre");
    }else if(!bilet.telefon.match("^[0-9]+$")){
      console.log("NUmarul trebuie sa contina cifre de la 0 la 9");
      error.push("Numarul trebuie sa contina cifre de la 0 la 9");
    }
    if(!bilet.email.includes("@gmail.com") && !bilet.email.includes("@yahoo.com")){
      console.log("Email invalid");
      error.push("Email invalid");
    }
                               //"/^[1-31]+$/^[1-12]+$/^[2000-9999]+$"
    //if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(bilet.data_inceput) || !/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(bilet.data_sfarsit)){
      //return false;
    //}
    //if(!bilet.data_inceput.match("^[1-31]+$/^[1-12]+$/^[2000-9999]+$") || !bilet.data_sfarsit.match("^[1-31]+$/^[1-12]+$/^[2000-9999]+$")){
    //  console.log("Format data invalid! Formatul corect: dd/mm/yyyy");
    //  error.push("Format data invalid! Formatul corect: dd/mm/yyyy");
    //}else if(bilet.data_inceput.localeCompare(bilet.data_sfarsit) != -1){
    //  console.log("Data de inceput trebuie sa fie mai mica decat data de sfarsit");
    //  error.push("Data de inceput trebuie sa fie mai mica decat data de sfarsit");
    //}

    //data_inceput = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    //data_sfarsit = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    //if (!(bilet.data_sfarsit.test(testDate)) || !(bilet.data_inceput.test(testDate))) {
    //  console.log("Format data invalid! Formatul corect: dd/mm/yyyy");
    //  error.push("Format data invalid! Formatul corect: dd/mm/yyyy");
    //}

    //if(!bilet.data_inceput.match("/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/") || !bilet.data_sfarsit.match("/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/")){
    //  console.log("Format data invalid! Formatul corect: dd/mm/yyyy");
    //  error.push("Format data invalid! Formatul corect: dd/mm/yyyy");
    //}

    var a =new Date(bilet.data_inceput);
    //a=a.format()
    var b = new Date(bilet.data_sfarsit);
    if(isNaN(a.valueOf()) || isNaN(b.valueOf())){
      console.log("Format data invalid!");
      error.push("Format data invalid!");
    }


    if(bilet.cnp.length != 13){
      console.log("CNP-ul trebuie sa aiba 10 cifre");
      error.push("CNP-ul trebuie sa aiba 10 cifre");
    }else if(!bilet.cnp.match("^[0-9]+$")){
      console.log("CNP-ul trebuie sa contina cifre de la 0 la 9");
      error.push("CNP-ul trebuie sa contina cifre de la 0 la 9");
    }
    if(bilet.varsta.length <1 || bilet.varsta.length >3){
      console.log("Varsta invalida!");
      error.push("Varsta invalida!");
    }else if(!bilet.varsta.match("^[0-9]+$")){
      console.log("Varsta trebuie sa fie compusa doar din cifre!");
      error.push("Varsta trebuie sa fie compusa doar din cifre");
    }
    //var yyyy = today.getFullYear();
    const mongoose = require('mongoose');

    const userSchema = new mongoose.Schema({
       email: {
      type: String,
      unique: true // `email` must be unique
  }
});
const User = mongoose.model('User', userSchema);


  }
 

  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume,
      prenume,
      telefon,
      cnp,
      email,
      data_inceput,
      data_sfarsit,
      varsta) VALUES (?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
